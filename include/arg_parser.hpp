#ifndef ARG_PARSER_HPP
#define ARG_PARSER_HPP

#include <string>
#include <vector>

class ArgParser {
public:
    ArgParser(const int &argc, char **argv);
    const std::string& get_arg(const std::string &flag, const bool& getValue = true) const;
    int get_int(const std::string &flag) const;
    const std::string& at(const uint &pos) const;
private:
    std::vector<std::string> tokens;
};

#endif // ARG_PARSER_HPP