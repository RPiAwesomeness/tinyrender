#ifndef TINY_RENDERER_SHAPES_H
#define TINY_RENDERER_SHAPES_H

#include "geometry.h"
#include "tgaimage.h"

const TGAColor WHITE = TGAColor(255, 255, 255, 255);
const TGAColor RED = TGAColor(255, 0, 0, 255);
const TGAColor GREEN = TGAColor(0, 255, 0, 255);

void line(Vec2i p0, Vec2i p1, TGAImage& img, TGAColor color);
void triangle(Vec2i t0, Vec2i t1, Vec2i t2, TGAImage &image, TGAColor color);

#endif //TINY_RENDERER_SHAPES_H
