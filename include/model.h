#ifndef __MODEL_H__
#define __MODEL_H__

#include <string>
#include <vector>

#include "geometry.h"

class Model {
private:
	std::vector<Vec3f> vertices;
	std::vector<std::vector<int>> faces;

public:
	Model(const char *filename);
	Model(std::string filename) : Model(filename.c_str()) {}
	~Model() {}

    int numVerts() const { return vertices.size(); }
	int numFaces() const { return faces.size(); }

    Vec3f vert(const int& idx) const { return vertices[idx]; }
	std::vector<int> face(const int& idx) const { return faces[idx]; }
};

#endif //__MODEL_H__
