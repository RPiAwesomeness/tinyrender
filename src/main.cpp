// std lib includes
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// Project includes
#include "arg_parser.hpp"
#include "config.h"
#include "model.h"
#include "shapes.h"
#include "tgaimage.h"

const uint32_t DEFAULT_WIDTH = 1024;
const uint32_t DEFAULT_HEIGHT = 1024;

const char* DEFAULT_OBJECT_PATH = "in.obj";
const char* DEFAULT_OUTPUT_PATH = "output.tga";

void parse_args(const int& argc, char** argv, std::string& objFilePath, std::string& resultPath, int& width,
                int& height, bool& clownsOccurreth) {
    // Parse out arguments
    ArgParser parser(argc, argv);

    if (!parser.get_arg("--help", false).empty()) {
        std::stringstream ss;
        ss << "Usage: tinyrenderer [options]" << std::endl
           << std::endl
           << "Options:\n"
           << "  --help\tDisplay this information\n"
           << "  --version\tDisplay the renderer version information\n"
           << "  -i\t\tSpecify input object file\t(default: in.tga)\n"
           << "  -o\t\tSpecify output TGA file\t\t(default: output.tga)\n"
           << "  -w\t\tSpecify output TGA image width\t(default: 1024)\n"
           << "  -h\t\tSpecify output TGA image height\t(default: 1024)" << std::endl;

        std::cout << ss.str() << std::endl;
        exit(0);
    } else if (!parser.get_arg("--version", false).empty()) {
        std::cout << "tinyrenderer " << tinyrenderer_VERSION_MAJOR << "." << tinyrenderer_VERSION_MINOR << "."
                  << tinyrenderer_VERSION_PATCH << std::endl;
        exit(0);
    }

    // Top secret easter egg mode boi
    clownsOccurreth = !parser.get_arg("--clowns", false).empty();

    objFilePath = parser.get_arg("-i");
    if (objFilePath.empty()) {
        objFilePath = DEFAULT_OBJECT_PATH;
    }

    resultPath = parser.get_arg("-o");
    if (resultPath.empty()) {
        resultPath = DEFAULT_OUTPUT_PATH;
    }

    // Note, width/height of TGA image must be at least 1x1
    width = parser.get_int("-w");
    if (width <= 0) {
        width = DEFAULT_WIDTH;
    }

    height = parser.get_int("-h");
    if (height <= 0) {
        height = DEFAULT_HEIGHT;
    }
}

void render(const Model& model, TGAImage& image, bool clownMode=false) {
    for (int i = 0; i < model.numFaces(); i++) {
        std::vector<int> face = model.face(i);
        Vec2i screenCoords[3];
        Vec3f worldCoords[3];
        Vec3f lightDir(0, 0, -1);

        for (int j = 0; j < 3; j++) {
            Vec3f v = model.vert(face[j]);
            screenCoords[j] = Vec2i((v.x + 1.0) * image.get_width() / 2.0, (v.y + 1.0) * image.get_height() / 2.0);
            worldCoords[j] = v;
        }

        Vec3f n = (worldCoords[2] - worldCoords[0]) ^(worldCoords[1] - worldCoords[0]);
        n.normalize();

        // Default to regular rendering, but if clown mode strikes...
        float intensity = n * lightDir;
        TGAColor color(intensity * 255, intensity * 255, intensity * 255, 255);

        if (clownMode) {
            // THE CLOWNS HAVE STRUCK!
            color = TGAColor(
                static_cast<unsigned char>(rand() % 255),
                static_cast<unsigned char>(rand() % 255),
                static_cast<unsigned char>(rand() % 255),
                255
            );
        }

        if (intensity > 0) {
            triangle(screenCoords[0], screenCoords[1], screenCoords[2], image, color);
        }
    }
}

int main(int argc, char** argv) {
    // Parse arguments
    std::string objFilePath, resultPath;
    int width, height;
    bool clownsOccurreth = false;
    parse_args(argc, argv, objFilePath, resultPath, width, height, clownsOccurreth);

    // Load model from file
    Model model(objFilePath.c_str());

    // Only try to render anything if there's anything to render!
    if (model.numFaces() == 0 || model.numVerts() == 0) {
        std::cout << "Invalid model loaded, no faces and/or no vertices defined!" << std::endl;
        return 1;
    }

    // Image to output to
    TGAImage image(width, height, TGAImage::RGB);

    // Render image into TGAImage object
    render(model, image, clownsOccurreth);

    // Set origin at bottom left
    image.flip_vertically();

    std::cout << "Rendered object to " << resultPath << "!" << std::endl;

    // Output rendered TGA
    image.write_tga_file(resultPath.c_str());

    return 0;
}