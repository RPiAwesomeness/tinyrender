#include <algorithm>
#include <sstream>

#include "arg_parser.hpp"

using namespace std;

ArgParser::ArgParser(const int& argc, char** argv) {
    for (int i = 1; i < argc; ++i) {
        this->tokens.push_back(string {argv[i]});
    }
}

/// Search arguments for flag specified and return associated value
/// Arguments:
///     flag: String of flag to search for
///     getValue: Boolean indicating whether this is a flag that is being checked for existence and not a value
const string& ArgParser::get_arg(const string& flag, const bool& getValue) const {
    vector<string>::const_iterator itr;
    static const string emptyString = "";   // Value to return if no matching flag/flag & value pair is set

    // Find the flag within the tokens, returning if there is a value available
    itr = find(this->tokens.begin(), this->tokens.end(), flag);
    if (itr != this->tokens.end()) {
        if (getValue && ++itr == this->tokens.end()) {
            return emptyString;
        }

        return *itr;
    }

    return emptyString;
}

/// Wrapper around ArgParser::get_opt that attempts to convert value to integer
int ArgParser::get_int(const string& flag) const {
    string val = this->get_arg(flag);
    stringstream intVal {val};

    // If there is no value for the integer flag requested, indicate as such
    if (val == "") return -1;

    int retVal;
    intVal >> retVal;

    return retVal;
}

/// Retrieves command line argument at index specified
const string& ArgParser::at(const uint& idx) const { return this->tokens.at(idx); }