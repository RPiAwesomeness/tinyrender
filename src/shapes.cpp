#include "geometry.h"
#include "tgaimage.h"

void line(Vec2i p0, Vec2i p1, TGAImage& img, TGAColor color) {
    bool steep = false;
    // If the line is steep, transpose the image
    if (std::abs(p0.x - p1.x) < std::abs(p0.y - p1.y)) {
        std::swap(p0.x, p0.y);
        std::swap(p1.x, p1.y);
        steep = true;
    }

    // Make it left-to-right, keeping x0 as starting point
    if (p0.x > p1.x) {
        std::swap(p0.x, p1.x);
        std::swap(p0.y, p1.y);
    }

    for (auto x = p0.x; x <= p1.x; x++) {
        float t = (x - p0.x) / static_cast<float>(p1.x - p0.x);
        int y = p0.y * (1.0 - t) + p1.y * t;
        if (steep) {
            img.set(y, x, color);
        } else {
            img.set(x, y, color);
        }
    }
}

void triangle(Vec2i t0, Vec2i t1, Vec2i t2, TGAImage &image, TGAColor color) {
    // Do not render degenerate triangles
    if (t0.y == t1.y && t0.y == t2.y) return;

    if (t0.y > t1.y) std::swap(t0, t1);
    if (t0.y > t2.y) std::swap(t0, t2);
    if (t1.y > t2.y) std::swap(t1, t2);

    int total_height = t2.y - t0.y;
    for (int i = 0; i < total_height; i++) {
        bool second_half = i > t1.y - t0.y || t1.y == t0.y;
        int segment_height = second_half ? t2.y - t1.y : t1.y - t0.y;
        float alpha = static_cast<float>(i) / total_height;
        // Note: with above conditions no division by zero here
        float beta  = static_cast<float>(i-(second_half ? t1.y - t0.y : 0)) / segment_height;
        Vec2i A = t0 + (t2 - t0) * alpha;
        Vec2i B = second_half ? t1 + (t2 - t1) * beta : t0 + (t1 - t0) * beta;
        if (A.x > B.x) std::swap(A, B);

        for (int j = A.x; j <= B.x; j++) {
            // Note, due to int casts t0.y + i != A.y
            image.set(j, t0.y + i, color);
        }
    }
}